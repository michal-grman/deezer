//
//  Assembly.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

@objc(DZHAssembly)
public class Assembly: NSObject {
    
    @objc public static func resolveRoot() -> UIViewController? {
        return Artist.SearchCoordinator().context?.navigationController
    }
}
