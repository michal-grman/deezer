//
//  ArtistCoordinator.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

extension Artist {
    
    public struct SearchCoordinator: CoordinatorConvertible, CoordinatorTransition {
    
        public func transition(to transition: Transition<Payload>) {
            TracksCoordinator(transition)
        }
        
    
        private weak var controller: ArtistSearchController?
        
        public var context: UIViewController? {
            return controller
        }
        
        @discardableResult
        public init(_ transition: Transition<Payload> = .none) {
        
            controller = Builder<ArtistSearchController>("Artist").resolve()
            controller?.viewModel = SearchViewModel(self)
        }
    }
}

