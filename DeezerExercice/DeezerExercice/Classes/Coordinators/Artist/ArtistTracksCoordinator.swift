//
//  ArtistTracksCoordinator.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

extension Artist {
    
    public struct TracksCoordinator: CoordinatorConvertible, CoordinatorTransition {
        
        public func transition(to transition: Transition<Track.Payload>) {
            Track.AudioCoordinator(transition)
        }
        
        
        private weak var controller: ArtistTracksController?
        
        public var context: UIViewController? {
            return controller
        }
        
        @discardableResult
        public init(_ transition: Transition<Payload>) {
            
            if case .push(let view, let data) = transition {
                
                controller = Builder<ArtistTracksController>("Artist").resolve("ArtistTracksController")
                controller?.viewModel = TracksViewModel(data, coordinator: self)
                
                guard let controller = controller else { fatalError("Controller failed to construct")}
                view.push(view: controller)
            }
        }
    }
}


