//
//  ArtistAudioCoordinator.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

extension Track {
    
    public struct AudioCoordinator: CoordinatorConvertible {
    
        private weak var controller: AVPlayerViewController?
        
        public var context: UIViewController? {
            return controller
        }
        
        @discardableResult
        public init(_ transition: Transition<Payload>) {
            
            if case .present(let view, let data) = transition, let url = data.preview {
                
                let player = AVPlayer(url: url)
                let controller = AVPlayerViewController()
                controller.player = player
                
                self.controller = controller
                view.present(view: controller) { [unowned player] in
                    player.play()
                }
            }
        }
    }
}
