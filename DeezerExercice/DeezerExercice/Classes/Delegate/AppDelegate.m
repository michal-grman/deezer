//
//  AppDelegate.m
//  DeezerExercice
//  Copyright (c) 2015 Deezer. All rights reserved.
//

#import "AppDelegate.h"
#import "DeezerExercice-Swift.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    
    [self.window setBackgroundColor:UIColor.whiteColor];
    [self.window makeKeyAndVisible];
    [self.window setRootViewController:[DZHAssembly.class resolveRoot]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

@end
