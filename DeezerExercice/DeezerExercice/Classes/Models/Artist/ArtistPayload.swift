//
//  ArtistPayload.swift
//  DeezerExercice
//
//  Created by Michal Grman on 05/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

extension Artist {
    
    public struct Payload: PayloadConvertible {
        
        public var id: String?
        public var name: String?
        public var picture: URL?
        public var tracks: URL?
        
        public init(usingJson json: [String : Any]) {
            
            self.id = json["id"] as? String;
            self.name = json["name"] as? String;
            self.picture = URL(string: (json["picture"] as? String) ?? "");
            self.tracks = URL(string: (json["tracklist"] as? String) ?? "");
        }
    }
}
