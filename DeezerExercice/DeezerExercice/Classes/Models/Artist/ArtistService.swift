//
//  ArtistService.swift
//  DeezerExercice
//
//  Created by Michal Grman on 05/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

extension Artist {
    public struct Service {
        public func search(using name:String, onComplete complete:@escaping ((_ result: Result<[Payload], HttpError>) -> ())) {
            if let url = API.search(name: name).url {
                HttpService().get(using: url, middleware: PayloadSequenceMiddleware<[Payload]>("data"), onComplete: complete)
            } else {
                complete(.failure(error: .noEndpoint))
            }
        }
    }
}
