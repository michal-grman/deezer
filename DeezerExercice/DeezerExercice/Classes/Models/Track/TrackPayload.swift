//
//  TrackPayload.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

extension Track {
    
    public struct Payload: PayloadConvertible {
        
        public var id: String?
        public var name: String?
        public var preview: URL?
        
        public init(usingJson json: [String : Any]) {
            
            self.id = json["id"] as? String;
            self.name = json["title"] as? String;
            self.preview = URL(string: (json["preview"] as? String) ?? "");
        }
    }
}
