//
//  TrackService.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

extension Track {
    public struct Service {
        public func tracks(using artist:Artist.Payload, onComplete complete:@escaping ((_ result: Result<[Payload], HttpError>) -> ())) {
            if let url = API.tracks(artist: artist).url {
                HttpService().get(using: url, middleware: PayloadSequenceMiddleware<[Payload]>("data"), onComplete: complete)
            } else {
                complete(.failure(error: .noEndpoint))
            }
        }
    }
}
