//
//  HttpService.swift
//  DeezerExercice
//
//  Created by Michal Grman on 05/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

public struct HttpService<M: Middleware>{

    public func get(
        using url: URL,
        middleware: M,
        onComplete complete:@escaping ((_ result: Result<M.Value, HttpError>) -> Void)
    ) {
        DispatchQueue.global(qos: .background).async {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                do {
                    if let error = error { throw error }
                    guard let data = data else { throw HttpError.noData }
                  
                    complete(.success(value:try middleware.decode(data)))
                } catch let error as HttpError {
                    complete(.failure(error: error))
                } catch (let error) {
                    complete(.failure(error: .underlaying(error: error)))
                }
            }.resume()
        }
    }
}
