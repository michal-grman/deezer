//
//  Api.swift
//  DeezerExercice
//
//  Created by Michal Grman on 05/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

public enum API {
    
    private static let root: String = "http://api.deezer.com";
    
    case search(name: String)
    case tracks(artist: Artist.Payload)
    
    public var url: URL? {
        switch self {
        case .search(let name):
            return URL(string:"\(API.root)/search/artist?q=\(name)")
        case .tracks(let artist):
            return artist.tracks
        }
    }
}
