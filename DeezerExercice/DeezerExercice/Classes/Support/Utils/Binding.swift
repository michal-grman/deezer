//
//  Binding.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit
import Foundation

protocol ImageAsyncProvider: class {
    func async(url:URL, onComplete complete: @escaping ((_ result: Result<UIImage, NoError>) -> ()))
}

extension ImageAsyncProvider {
    public func async(url:URL, onComplete complete: @escaping ((_ result: Result<UIImage, NoError>) -> ())) {
        HttpService().get(using: url, middleware: DataMiddleware()) { result in
            if case .success(let value) = result, let image = UIImage(data: value) {
                DispatchQueue.main.async {
                    complete(.success(value: image))
                }
            }
        }
    }
}

public protocol ViewBindingProvider: class {}

extension ViewBindingProvider {
    public var binding: ViewBinding<Self> {
        return ViewBinding(self)
    }
}

public struct ViewBinding<Base> {
    public let base: Base
    
    fileprivate init(_ base: Base) {
        self.base = base
    }
}

