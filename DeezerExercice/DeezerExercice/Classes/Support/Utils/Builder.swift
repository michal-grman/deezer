//
//  UIBuilder.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//


import UIKit

public struct Builder<C: UIViewController> {
    
    private let storyboard: UIStoryboard
    public init(_ name: String, bundle: Bundle? = nil) {
        self.storyboard = UIStoryboard(name: name, bundle: bundle)
    }
    
    public func resolve(_ identifier: String? = nil) -> C? {
        guard let identifier = identifier else {
            let controller = self.storyboard.instantiateInitialViewController()
            if let controller = controller as? UINavigationController {
                return controller.topViewController as? C
            }
            return controller as? C
        }
        return self.storyboard.instantiateViewController(withIdentifier: identifier) as? C
    }
}
