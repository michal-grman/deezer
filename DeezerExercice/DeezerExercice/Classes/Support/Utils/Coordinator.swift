//
//  Coordinator.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

public protocol CoordinatorTransition: Presentable {
    associatedtype Output
    func transition(to transition: Transition<Output>)
}


public protocol CoordinatorConvertible {
    associatedtype Input
    init(_ transition: Transition<Input>)
}
