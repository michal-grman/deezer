//
//  Error.swift
//  DeezerExercice
//
//  Created by Michal Grman on 05/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

public enum NoError : Error {}

public enum MiddlewareError : Error {
    case failed
}

public enum HttpError : Error, CustomStringConvertible {
    case noData
    case noEndpoint

    case underlaying(error: Error)
    
    public var description: String {
        switch self {
        case .noData:        return "Missing data"
        case .noEndpoint:    return "Missing endpoint"
        case .underlaying(let error):
            return "Underlaying error: \(error.localizedDescription)"
        }
    }
}
