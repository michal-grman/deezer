//
//  Invocation.swift
//  DeezerExercice
//
//  Created by Michal Grman on 08/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

func debounce<A>(
    delay: DispatchTimeInterval,
    queue: DispatchQueue = .main, 
    action: @escaping ((A) -> ())
) -> (A) -> () {
    var currentWorkItem: DispatchWorkItem?
    return { (a: A) in
        currentWorkItem?.cancel()
        currentWorkItem = DispatchWorkItem { action(a) }
        queue.asyncAfter(deadline: .now() + delay, execute: currentWorkItem!)
    }
}

func debounce<A,B>(
    delay: DispatchTimeInterval,
    queue: DispatchQueue = .main,
    action: @escaping ((A, B) -> ())
    ) -> (A, B) -> () {
    var currentWorkItem: DispatchWorkItem?
    return { (a: A, b: B) in
        currentWorkItem?.cancel()
        currentWorkItem = DispatchWorkItem { action(a, b) }
        queue.asyncAfter(deadline: .now() + delay, execute: currentWorkItem!)
    }
}

