//
//  Middleware.swift
//  DeezerExercice
//
//  Created by Michal Grman on 05/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import Foundation

public protocol Middleware {
    associatedtype Value
    func decode(_ data: Data) throws -> Value
}

public struct DataMiddleware: Middleware {
    public func decode(_ data: Data) throws -> Data {
        return data
    }
}

public protocol PayloadConvertible {
    init(usingJson json: [String: Any])
}

public struct PayloadMiddleware<Value: PayloadConvertible> : Middleware {
    
    private let keypath: String?
    public init(_ keypath: String?) {
        self.keypath = keypath
    }
    
    public func decode(_ data: Data) throws -> Value {
        guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
            throw MiddlewareError.failed
        }
        
        if let key = keypath, let object = json[key] as? [String: Any] {
            return Value(usingJson: object)
        }
        
        return Value(usingJson: json)
    }
}

public struct PayloadSequenceMiddleware<Value: Sequence> : Middleware where Value.Iterator.Element: PayloadConvertible {
    
    private let keypath: String
    public init(_ keypath: String) {
        self.keypath = keypath
    }
    
    public func decode(_ data: Data) throws -> Value {
        guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
            throw MiddlewareError.failed
        }
        guard let objects = json[keypath] as? [[String: Any]]  else {
            throw MiddlewareError.failed
        }
        return objects.map{ Value.Iterator.Element(usingJson: $0) } as! Value
    }
}
