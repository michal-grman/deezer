//
//  Presentable.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

public protocol Presentable {
    
    var context: UIViewController? { get }
    func push(view: Presentable)
    func present(view: Presentable, complete: @escaping () -> ())
}

extension Presentable {
    
    public var context: UIViewController? {
        return nil
    }
    
    public func push(view: Presentable) {
        
        guard let nav = self.context?.navigationController, let context = view.context else {
            fatalError("Forgot to embed navigation controller!")
        }
        nav.pushViewController(context, animated: true)
    }
    
    public func present(view: Presentable, complete: @escaping () -> ()) {
        
        guard let parent = self.context, let context = view.context else {
            fatalError("Forgot parent controller!")
        }
        
        parent.present(context, animated: true) {
            complete()
        }
    }
}

extension UIViewController: Presentable {
    
    public var context: UIViewController? {
        return self
    }
    
    public func push(view: Presentable) {
        
        guard let nav = self.context?.navigationController, let context = view.context else {
            fatalError("Forgot to embed navigation controller!")
        }
        nav.pushViewController(context, animated: true)
    }
    
    public func present(view: Presentable, complete: @escaping () -> ()) {
        
        guard let parent = self.context, let context = view.context else {
            fatalError("Forgot parent controller!")
        }
        
        parent.present(context, animated: true) {
            complete()
        }
    }
}
