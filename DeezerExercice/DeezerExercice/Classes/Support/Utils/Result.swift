//
//  Result.swift
//  DeezerExercice
//
//  Created by Michal Grman on 05/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

public enum Result<Value, Error> {
    case success(value:Value)
    case failure(error:Error)
}

