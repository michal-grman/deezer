//
//  Transition.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

public enum Transition<Value> {
    
    case none
    case push(view: Presentable, data: Value)
    case present(view: Presentable, data: Value)
}

