//
//  ArtistCoordinator.swift
//  DeezerExercice
//
//  Created by Michal Grman on 05/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

extension Artist {
    
    public class SearchViewModel<Coordinator: CoordinatorTransition> where Coordinator.Output == Payload {
        
        private let coordinator: Coordinator
        private let service: Service
        private var results: [Payload] = []
    
        public var artistCount:Int {
            return results.count
        }
        
        public init(_ coordinator: Coordinator) {
            self.service = Service()
            self.coordinator = coordinator
        }
        
        deinit {
            print("Deallocated VM \(self)")
        }
 
        public func search(using name: String, complete: @escaping ()->()) -> Void {
            self.service.search(using: name) { [unowned self] (result) in
                DispatchQueue.main.async {
                    if case .success(let value) = result {
                        self.results = value
                        complete()
                    }
                }
            }
        }

        public func artist(at indexPath: IndexPath) -> Payload {
            return results[indexPath.item]
        }
        
        public func detailArtist(at indexPath: IndexPath) {
            let result = self.artist(at: indexPath)
            self.coordinator.transition(to: .push(view:self.coordinator, data:result))
        }
    }
}
