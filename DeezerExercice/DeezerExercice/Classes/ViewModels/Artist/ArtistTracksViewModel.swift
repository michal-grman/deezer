//
//  ArtistTracksViewModel.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

extension Artist {
    
    public class TracksViewModel<Coordinator: CoordinatorTransition> where Coordinator.Output == Track.Payload {
        
        private let coordinator: Coordinator
        private let service: Track.Service
        private var results: [Track.Payload] = []
        
        public let artist: Payload
        
        public var trackCount:Int {
            return results.count
        }
        
        public init(_ artist:Payload, coordinator: Coordinator) {
            self.service = Track.Service()
            self.artist = artist
            self.coordinator = coordinator
        }
        
        deinit {
            print("Deallocated VM \(self)")
        }
        
        public func tracks(_ complete: @escaping ()->()) -> Void {
            self.service.tracks(using: self.artist) { [unowned self] (result) in
                DispatchQueue.main.async {
                    if case .success(let value) = result {
                        self.results = value
                        complete()
                    }
                }
            }
        }
        
        public func track(at indexPath: IndexPath) -> Track.Payload {
            return results[indexPath.item]
        }
        
        public func previewTrack(at indexPath: IndexPath) {
            let result = self.track(at: indexPath)
            self.coordinator.transition(to: .present(view:self.coordinator, data:result))
        }
    }
}
