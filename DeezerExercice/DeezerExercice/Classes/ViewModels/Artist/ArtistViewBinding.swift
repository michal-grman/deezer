//
//  ArtistGridCellBinding.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit


extension ViewBinding where Base: ArtistResultCell {
 
    public func bind(_ artist: Artist.Payload) {
        
        if let url = artist.picture {
            self.base.picture?.binding.async(url: url)
        }
        
        self.base.title?.text = artist.name
    }
}

extension ViewBinding where Base: TrackResultCell {
    
    public func bind(_ track: Track.Payload) {
        self.base.title?.text = track.name
    }
}

extension ViewBinding where Base: ArtistTracksController {
    
    public func bind(_ artist: Artist.Payload) {
        self.base.title = artist.name
    }
}
