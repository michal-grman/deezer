//
//  BaseViewBinding.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit


extension UIImageView : ViewBindingProvider, ImageAsyncProvider {}

extension ViewBinding where Base: UIImageView {
    
    public func async(url: URL) {
        self.base.async(url: url) { result in
            if case .success(let value) = result {
                self.base.image = value
            }
        }
    }
}
