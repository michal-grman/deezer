//
//  ArtistSearchController.swift
//  DeezerExercice
//
//  Created by Michal Grman on 05/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

public class ArtistSearchController: UIViewController {

    @IBOutlet weak var search: UISearchBar?
    @IBOutlet weak var grid: UICollectionView?
    
    public var viewModel:Artist.SearchViewModel<Artist.SearchCoordinator>?
    public var searchAction: ((String) -> ())?
    
    deinit {
        print("Deallocated VC \(self)")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        
        guard let _ = viewModel else {
            fatalError("Forget to inject viewmodel!")
        }
        
        let executable: ((String) -> ()) = { [unowned self] text in
            print(text)
            self.viewModel!.search(using: text) { [unowned self] in
                self.grid?.reloadData()
            }
        }
        self.searchAction = debounce(delay: .milliseconds(300), action: executable)
    }
}

extension ArtistSearchController: UISearchBarDelegate {
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchAction?(searchText)
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension ArtistSearchController: UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel!.artistCount
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArtistResultCell.identifier, for: indexPath) as? ArtistResultCell else {
            fatalError("Wrong cell was registered")
        }
        
        cell.binding.bind(viewModel!.artist(at: indexPath))
        return cell
    }
}

extension ArtistSearchController: UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        viewModel!.detailArtist(at: indexPath)
    }
}
