//
//  ArtistTracksController.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

public class ArtistTracksController: UIViewController, ViewBindingProvider {

    @IBOutlet weak var grid: UICollectionView?
    
    public var viewModel:Artist.TracksViewModel<Artist.TracksCoordinator>?
    
    deinit {
        print("Deallocated VC \(self)")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        guard let vm = viewModel else {
            fatalError("Forget to inject viewmodel!")
        }
        self.binding.bind(vm.artist)
        vm.tracks { [unowned self] in
            self.grid?.reloadData()
        }
    }
}

extension ArtistTracksController: UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel!.trackCount
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TrackResultCell.identifier, for: indexPath) as? TrackResultCell else {
            fatalError("Wrong cell was registered")
        }
        
        cell.binding.bind(viewModel!.track(at: indexPath))
        return cell
    }
}

extension ArtistTracksController: UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        viewModel!.previewTrack(at: indexPath)
    }
}
