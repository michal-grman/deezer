//
//  ArtistResultCell.swift
//  DeezerExercice
//
//  Created by Michal Grman on 05/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

open class ArtistResultCell: UICollectionViewCell, ViewBindingProvider {
 
    public static let identifier: String = "\(ArtistResultCell.self)"
    
    @IBOutlet weak var picture: UIImageView?
    @IBOutlet weak var title: UILabel?
}


