//
//  TrackResultCell.swift
//  DeezerExercice
//
//  Created by Michal Grman on 06/12/2017.
//  Copyright © 2017 Deezer. All rights reserved.
//

import UIKit

open class TrackResultCell: UICollectionViewCell, ViewBindingProvider {
    
    public static let identifier: String = "\(TrackResultCell.self)"

    @IBOutlet weak var title: UILabel?
}
